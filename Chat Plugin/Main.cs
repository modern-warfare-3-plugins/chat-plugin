﻿using Addon;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ChatPlugin
{
    public class Main : CPlugin
    {
        private readonly Dictionary<string, string>     _groups                     = new Dictionary<string, string>();
        private readonly Dictionary<string, string>     _xuids                      = new Dictionary<string, string>();
        private readonly Dictionary<string, bool>       _chat                       = new Dictionary<string, bool>();

        private const string                            DefaultGroups               = "Admin,Moderator";
        private const string                            InvalidGroupValueRegExp     = @"\*(EVERYONE|ALL)\*";

        private string[]                                _groupsToUse;
        private bool                                    _changeTeamNames;
        private bool                                    _togglechat;

        /// <summary>
        /// Executes when the MW3 server loads
        /// </summary>
        public override void OnServerLoad()
        {
            ServerPrint("[ChatPlugin] By 8Q4S8 loaded!");
            ServerPrint("[ChatPlugin] Updated by SgtLegend on February 14. Version 1.1");

            // Set the default state of the plugin
            bool.TryParse(GetServerCFG("ChatPlugin", "DisableChatPlugin", "false"), out _togglechat);
            bool.TryParse(GetServerCFG("ChatPlugin", "ChangeTeamNames", "false"), out _changeTeamNames);

            // Get the configuration values we need
            _groupsToUse = GetServerCFG("ChatPlugin", "GroupsToUse", DefaultGroups).Split(',');

            if (_groupsToUse.Length == 0) return;
            foreach (var group in _groupsToUse)
            {
                // Add the group to a storage array
                if (!_groups.ContainsKey(group))
                {
                    _groups.Add(group, GetServerCFG("ChatPlugin", "MessageFor" + group, "[^2{0}^7] ^7{1}^7: {2}"));
                }

                // Extract the xuids for the specified group
                var xuidsForGroup = GetServerCFG("Permission", group + "_xuids", "");

                if (Regex.Match(xuidsForGroup, InvalidGroupValueRegExp, RegexOptions.IgnoreCase).Success) continue;
                var xuids = xuidsForGroup.Split(',');

                if (xuids.Length == 0) continue;
                foreach (var xuid in xuids.Where(xuid => !_xuids.ContainsKey(xuid)))
                {
                    _xuids.Add(xuid, group);
                }
            }

            // Set the team names
            SetTeamNames();
        }

        /// <summary>
        /// Executes when the map changes
        /// </summary>
        public override void OnMapChange()
        {
            SetTeamNames();
        }

        /// <summary>
        /// Executes when the same map retarts
        /// </summary>
        public override void OnFastRestart()
        {
            SetTeamNames();
        }

        /// <summary>
        /// Executes when a player types something into the chat
        /// </summary>
        /// <param name="message"></param>
        /// <param name="client"></param>
        /// <param name="teamchat"></param>
        /// <returns></returns>
        public override ChatType OnSay(string message, ServerClient client, bool teamchat)
        {
            if (!message.StartsWith("!") && !_togglechat && !_chat[client.XUID] && _xuids.ContainsKey(client.XUID) &&
                ((_changeTeamNames && teamchat) || !teamchat))
            {
                var mg = _groups.Where(group => _xuids[client.XUID] == group.Key && !string.IsNullOrEmpty(group.Value));

                foreach (var group in mg)
                {
                    if (teamchat)
                    {
                        var teamMessage = GetServerCFG("ChatPlugin", string.Format("MessageFor{0}Team", group.Key),
                                                       "^8({0}^8){1}^7: {2}");

                        foreach (var c in GetClients().Where(c => c.Team == client.Team))
                        {
                            TellClient(c.ClientNum,
                                       string.Format(teamMessage,
                                                     GetDvar(string.Format("g_TeamName_{0}",
                                                                           (c.Team == Teams.Allies
                                                                                ? "Allies"
                                                                                : "Axis"))), client.Name, message),
                                       true);
                        }
                    }
                    else
                    {
                        ServerSay(string.Format(group.Value, group.Key, client.Name, message), true);
                    }

                    return ChatType.ChatNone;
                }
            }

            // Toggle the plugin for all the clients on the server
            if (message.ToLower() == "!chat")
            {
                _togglechat = !_togglechat;
                return ChatType.ChatNone;
            }

            // Toggle the plugin for the client that typed this command
            if (message.ToLower() == "!toggletag")
            {
                _chat[client.XUID] = !_chat[client.XUID];
                return ChatType.ChatNone;
            }

            return ChatType.ChatContinue;
        }

        /// <summary>
        /// Executes when a player joins the server
        /// </summary>
        /// <param name="client"></param>
        public override void OnPlayerConnect(ServerClient client)
        {
            if (!_chat.ContainsKey(client.XUID))
            {
                _chat.Add(client.XUID, false);
            }

            AddClientToChatXuiDsList(client.XUID);
        }

        /// <summary>
        /// Tries to find the group for the user that has just joined the server
        /// </summary>
        /// <param name="xuid"></param>
        private void AddClientToChatXuiDsList(string xuid)
        {
            string[] userGroups = GetServerCFG("ChatPlugin", "GroupsToUse", DefaultGroups).Split(',');

            if (userGroups.Length == 0) return;
            foreach (string group in userGroups)
            {
                string xuidsForGroup = GetServerCFG("Permission", group + "_xuids", "");

                if (Regex.Match(xuidsForGroup, InvalidGroupValueRegExp, RegexOptions.IgnoreCase).Success &&
                    !_xuids.ContainsKey(xuid))
                {
                    _xuids.Add(xuid, group);
                }
            }
        }

        /// <summary>
        /// Sets the team names for the human and infected teams
        /// </summary>
        private void SetTeamNames()
        {
            if (!_changeTeamNames) return;

            SetDvar("g_TeamName_Allies", GetServerCFG("ChatPlugin", "TeamAllies", "^2Humans"));
            SetDvar("g_TeamName_Axis", GetServerCFG("ChatPlugin", "TeamAxis", "^1Infected"));
        }
    }
}